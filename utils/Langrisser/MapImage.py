from ._main import DIRS2,resPath
from PIL import Image, ImageFont, ImageDraw
import io
import os
import json 

Inkfree=os.path.join(resPath,'Inkfree.ttf')
ebrima=os.path.join(resPath,'ebrima.ttf')
##### SETTINGS ################################################################################
DEBUG=False #show printed map on command
TILE_RESOLUTION=48
BORDER_WIDTH=1
HEIGHTSTEPS = 20
FONT={
	'TEXT':	 ImageFont.truetype(font=Inkfree, size=int(TILE_RESOLUTION/4), index=0, encoding=''),
	'HEADER':   ImageFont.truetype(font=ebrima, size=int(TILE_RESOLUTION/3), index=0, encoding='')
}
COLOR={
	'PARTY':		(65,105,225),
	'ALLY':		 	(58,190,98),
	'ENEMY':		(166,16,30),
	'HIDDEN':		(166,16,30),
	'TREASURE':		(241, 58, 255),
	'EVENT':		(244, 140, 66),
	'HEIGHT_MIN':	(245,245,220),
	'HEIGHT_MAX':	(139,69,19),
	'HEADER':		(0,0,0),
	'HEADER_BORDER':(255,255,255),
	'BORDER':		(127,127,127),
	'TEXT':			(0,0,0),
	'TEXT_HEADER':  (255,255,255),
}

def Test():
	global DEBUG
	DEBUG=True
	MapImage(4303)

##### CONSTANTS #######################################################


##### QUEST PAGE DECISION #############################################

##### CREATE MAP IMAGE '''''''''''''''''''''''''''''''''''''''''''''''''''''''''
def MapImage(battle):
	#	Battle or Battle ID in
	if type(battle) == int:
		battle = DIRS2['Battle'][battle]
	field=DIRS2['Battlefield'][battle['BattlefieldID']]
	width=field['Width']
	height=field['Height']

	tiles = ConvertData(battle)
	img = DrawImage(tiles,width,height)

	#img.save('my.png')
	if DEBUG:
		img.show()

	#convert to binary
	imgByteArr = io.BytesIO()
	img.save(imgByteArr, format='PNG')
	#imgByteArr = imgByteArr.getvalue()
	imgByteArr.seek(0)
	return imgByteArr

def Coloring(tile):
	if tile['type'] in COLOR:
		return(COLOR[tile['type']])
	elif tile['type'] == 'Terrain':
		terrain=DIRS2['Terrain'][tile['terrain']]
		tkey=	tuple([terrain[key] for key in ["MovePointRide","MovePointWalk","MovePointWater","MovePointFly","MovePointFieldArmy","BattleBonus","Damage"]])
		if tkey in terrains and bool(terrains[tkey]):
			return terrains[tkey]
		else:
			return (0,0,0)
		#terrain = DIRS2['Terrain'][tile['terrain']]
		#return tuple(terrain[key] for key in ['ColorR','ColorG','ColorB'])
	return(127,127,127)

def ConvertData(battle):
	field=DIRS2['Battlefield'][battle['BattlefieldID']]
	width=field['Width']
	height=field['Height']

	#CONVERT ARRAY TO MATRIX
	AIO={
		(x,y):{
				'terrain': field['Terrains'][y*width+x],
				'type':	'Terrain'
		}
		for x in range(width)
		for y in range(height)
	}

	#ADD SPAWNS TO MAP
	for spawn in battle["AllyPositions"]:
		AIO[(spawn['X'],spawn['Y'])]['type'] = 'ALLY'

	for spawn in battle["EnemyActors"]:
		AIO[(spawn['X'],spawn['Y'])]['type'] = 'ENEMY'

	if 'NpcActors' in battle:
		for spawn in battle["NpcActors"]:
			AIO[(spawn['X'],spawn['Y'])]['type'] = 'NPC'

	if 'BattleTreasuresID' in battle:
		for spawn in battle["BattleTreasuresID"]:
			treasure = DIRS2['BattleTreasure'][spawn]
			spawn = treasure['ModelPosition'][0]
			AIO[(spawn['X'],spawn['Y'])]['type'] = 'TREASURE'

	if 'EventTriggersID' in battle:
		for et_id in battle['EventTriggersID']:
			et = DIRS2['BattleEventTrigger'][et_id]
			#mark locations
			try:
				for mark in et['Param3']:
					AIO[(mark['X'],mark['Y'])]['type'] = 'EVENT'
				#spawns?
				for ea_id in et['ActionsID']:
					ea = DIRS2['BattleEventAction'][ea_id]
					if 'Param2' in ea and len(ea['Param2']) and all([hid in DIRS2['Hero'] for hid in ea['Param2']]):
						for mark in ea['Param3']:
							AIO[(mark['X'],mark['Y'])]['type'] = 'HIDDEN'
			except:
				pass

	return AIO

def DrawImage(tiles,width,height):
	border={
		'use':	  True,
		'width':	BORDER_WIDTH
		}
	#upsize and add borders
	data=[]
	#top COLOR['HEADER']
	border['length']=border['width']*width + (width+1)*TILE_RESOLUTION
	for x in range(width+1):
		data+=[COLOR['HEADER']]*TILE_RESOLUTION + ([COLOR['HEADER_BORDER']]*border['width'] if x!=width else [])
	data+= data*(TILE_RESOLUTION-1) + [COLOR['HEADER_BORDER']]*border['length']

	#define horizontal border
	border_hori=[COLOR['HEADER_BORDER']]*(TILE_RESOLUTION+border['width'])+[COLOR['BORDER']]*(border['length']-border['width']-TILE_RESOLUTION)
	border_hori+=border_hori*(border['width']-1)
	#
	for y in range(height):
		#add row COLOR['HEADER'] with COLOR['HEADER'] border
		line=[COLOR['HEADER']]*TILE_RESOLUTION + [COLOR['HEADER_BORDER']]*border['width']
		for x in range(width):
			#add normal tiles with border 
			line+=[Coloring(tiles[(x,y)])]*TILE_RESOLUTION + ([COLOR['BORDER']]*border['width'] if x+1!=width else [])
		#add line multiple times for the right height
		data+=line*TILE_RESOLUTION + ( border_hori if y+1!=height else [])
	#define image
	img = Image.new('RGB', ((width+1)*TILE_RESOLUTION+(width)*border['width'], (height+1)*TILE_RESOLUTION+(height)*border['width']), "white")
	#add data
	img.putdata(data)

	return WriteText(tiles,img,width,height,border)

def WriteTextToTile(draw, text, tfont, tcolor, x , y):
	w, h = draw.textsize(text,tfont)
	if '\n' not in text:
		px=x*(TILE_RESOLUTION+BORDER_WIDTH) + int(TILE_RESOLUTION/2 - w/2)
		py=y*(TILE_RESOLUTION+BORDER_WIDTH) + int(TILE_RESOLUTION/2 - h/2)
		draw.text(
			(px,py),  # Coordinates
			text,  # Text
			tcolor,
			tfont
			)
	else:
		lines=text.split('\n')
		#center position
		cx = x*(TILE_RESOLUTION+BORDER_WIDTH) + (TILE_RESOLUTION/2)
		#upper position
		uy = y*(TILE_RESOLUTION+BORDER_WIDTH) + (TILE_RESOLUTION/2) - (h/2)
		for i,line in enumerate(lines[::-1]):
			wl, hl = draw.textsize(line,tfont)
			draw.text(
				(	# Coordinates
					int(cx-(wl/2)),
					int(uy+(h*i/len(lines)))
				),  
				line,  # Text
				tcolor,
				tfont
				)

def WriteText(AIO,img,width,height,border):
	draw=ImageDraw.Draw(img)
	####	add header values
	tcolor=COLOR['TEXT_HEADER']
	tfont=FONT['HEADER']
	for y in range(height):
		WriteTextToTile(draw,chr(65+y), tfont, tcolor, 0, y+1)
	for x in range(width):
		WriteTextToTile(draw,str(x), tfont, tcolor, x+1, 0)

##### WRITE TEXT ########################################################################
	
	for y in range(height):
		for x in range(width):
			tile = AIO[(x,y)]
			text = tile['type'][0] if tile['type'] in ['ALLY','ENEMY','NPC'] else str(tile['terrain'])
			tcolor=COLOR['TEXT']
			tfont=FONT['TEXT']
			WriteTextToTile(draw,text, tfont,tcolor, x+1, y+1)

	return img

######	Terrains
terrains={
	#normal terrain 	~	light brown/yellow
	(1, 1, 1, 1, 1, 0, 0)	:	(255, 239, 188),
	#river
	(2, 2, 1, 1, 1, 0, 0)	:	(102, 178, 255),
	#shallow water
	(3, 2, 1, 1, 1, 0, 0)	:	(53, 152, 252),
	#deep water
	(3, 3, 1, 1, 1, 0, 0)	:	(12, 129, 24),
	#forest
	(3, 1, 2, 1, 1, 20, 0)	:	(1, 165, 4),
	#mountain
	(3, 2, 3, 1, 1, 10, 0)	:	(198, 116, 1),
	#uncrossable
	(0, 0, 0, 0, 0, 0, 0)	:	(255, 255, 255),
	#wall
	(3, 2, 3, 1, 1, 30, 0)	:	(127, 126, 126),
	#flier only
	(0, 0, 0, 1, 0, 0, 0)	:	(237, 254, 255),
	#pillar/tent, flier only
	(0, 0, 0, 1, 0, 5, 0)	:	(),
	#wallish/fence
	(3, 2, 2, 1, 1, 5, 0)	:	(),
	#cargo
	(2, 1, 2, 1, 1, 5, 0)	:	(),
	#fire
	(1, 1, 1, 1, 1, 0, 100)	:	(255, 81, 12),
	#swamp
	(2, 1, 1, 1, 1, 0, 0)	:	(201, 255, 102),
	#throne
	(3, 2, 3, 1, 1, 40, 0)	:	(),
	#fire
	(1, 1, 1, 1, 1, 0, 30)	:	(255, 81, 12),
	#desert
	(2, 1, 2, 1, 1, 0, 0)	:	(255, 227, 137),
	#trenches
	(2, 1, 1, 1, 1, 5, 0)	:	(),
	(1, 1, 1, 1, 1, 0, 50)	:	(),
	(1, 1, 1, 1, 1, 0, -100)	:	(),
	(0, 0, 0, 0, 0, 20, 0)	:	(),
	(1, 1, 1, 1, 1, 0, 20)	:	(),
}


if __name__ == '__main__':
	Test()

'''
	normal terrain
	"(1, 1, 1, 1, 1, 0, 0)": [
		"Path",
		"Bridge",
		"Indoors",
		"Grassland",
		"Wooden Bridge",
		"Temple",
		"Cave",
		"Path",
		"Deck",
		"Snow",
		"Indoors",
		"Temple",
		"Luna Command Point",
		"Stone Tablet",
		"Rift",
		"Phoenix Rift",
		"室内",
		"室内",
		"撤离点",
		"不谐音符",
		"室内",
		"时空裂隙",
		"时空裂隙",
		"草地",
		"撤退点",
		"封印点",
		"测试",
		"测试",
		"测试",
		"测试",
		"测试",
		"测试",
		"测试",
		"测试",
		"测试",
		"测试",
		"测试",
		"测试",
		"测试",
		"测试",
		"测试",
		"测试",
		"测试"
	],

	river
	],
	"(2, 2, 1, 1, 1, 0, 0)": [
		"River"
	],

	shallow water
	"(3, 2, 1, 1, 1, 0, 0)": [
		"Water",
		"Flood"
	],

	deep water
	"(3, 3, 1, 1, 1, 0, 0)": [
		"Depths"
	],

	forest
	"(3, 1, 2, 1, 1, 20, 0)": [
		"Forest",
		"树林"
	],

	mountain
	"(3, 2, 3, 1, 1, 10, 0)": [
		"Mountain",
		"Sand Dunes"
	],

	uncrossable
	"(0, 0, 0, 0, 0, 0, 0)": [
		"Wall",
		"Cave Wall",
		"City Gates",
		"City Gates",
		"City Gates",
		"City Gates",
		"旋涡",
		"火柱",
		"激流",
		"石碑",
		"石碑"
	],

	wall
	"(3, 2, 3, 1, 1, 30, 0)": [
		"City Wall"
	],

	flier only
	"(0, 0, 0, 1, 0, 0, 0)": [
		"Sky",
		"Cliff",
		"Lava"
	],
	"(0, 0, 0, 1, 0, 5, 0)": [
		"Stone Pillar",
		"Tent"
	],

	wallish
	"(3, 2, 2, 1, 1, 5, 0)": [
		"Fence",
		"Fence"
	],

	cargo
	"(2, 1, 2, 1, 1, 5, 0)": [
		"Tomb",
		"Cargo"
	],

	fire
	"(1, 1, 1, 1, 1, 0, 100)": [
		"Flames",
		"毒雾"
	],

	swamp
	"(2, 1, 1, 1, 1, 0, 0)": [
		"Swamp",
		"冰面"
	],
	"(3, 2, 3, 1, 1, 40, 0)": [
		"Throne"
	],
	"(1, 1, 1, 1, 1, 0, 30)": [
		"Flames"
	],
	"(2, 1, 2, 1, 1, 0, 0)": [
		"Desert"
	],
	"(2, 1, 1, 1, 1, 5, 0)": [
		"Trenches"
	],
	"(1, 1, 1, 1, 1, 0, 50)": [
		"Flames",
		"召唤阵"
	"(1, 1, 1, 1, 1, 0, -100)": [
		"治愈迷雾"
	],
	"(0, 0, 0, 0, 0, 20, 0)": [
		"树林",
		"召唤阵"
	],
	"(1, 1, 1, 1, 1, 0, 20)": [
		"逆波",
		"火焰"
	]
}
'''
