from ._main import DIRS,Embed, DIRS2, gitlink, convert_HTML

def JobMats(id):
	#get gear dir
	gear=DIRS2['JobMaterial'][id]
	#create basic embed
	embed= Embed(
		title=gear['Name'], #page name
		#url=gear['url']  #page link
		)
	embed.set_thumbnail(url=gitlink(gear['Icon']))
	
	rifts=[]
	guild_store=False
	for path in gear['GetPathList']:
		if path['PathType'] == 'GetPathType_Rift':
			rifts.append(DIRS2['RiftLevel'][path['ID']]['NameNum']+('E' if path['ID']>5000 else ''))
		elif path['PathType'] == 'GetPathType_GuildStore':
			guild_store=True

	fields=[
		{'name':'Rarity'   ,'value':DIRS2['Rank'][gear['Rank']]['Desc'] ,'inline':True},
		{'name':'Description'   ,'value': convert_HTML(gear['Desc']) ,'inline':True},
		{'name':'Rifts',		'value':	'\n'.join(rifts),	'inline':True},
		{'name':'Guild Store',	'value':	str(guild_store),	'inline':True},
	]
	
	embed.ConvertFields(fields)
	return embed