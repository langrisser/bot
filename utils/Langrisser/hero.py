from ._main import DIRS, Embed, createTable, DIRS2, gitlink, SkillDescriptionAIO, convert_HTML
from tabulate import tabulate

def Hero2(id,page='main'):
	#get hero dir
	hero=DIRS2['Hero'][id]
	#create basic embed
	embed= Embed(
		title=page.title()
		)
	embed.set_author(name=hero['NameEng'])
	try:
		embed.set_thumbnail(url=gitlink(DIRS2['CharImage'][hero['CharImageID']]['CardHeadImage']))
	except:
		pass
	embed.set_footer(text='hero2')

	if page=='main':
		embed.ConvertFields(main2(hero))

	elif page=='lore':
		embed.ConvertFields(lore2(hero))

	elif page=='image':
		embed.description=gitlink('/ui/heropainting/%s/%s.png'%(DIRS2['Rank'][hero['Rank']]['Desc'],hero['Name']))
		embed.set_image(url=gitlink('/ui/heropainting/%s/%s.png'%(DIRS2['Rank'][hero['Rank']]['Desc'],hero['Name'])))

	elif page=='skills':
		embed.description=skills2(hero)

	elif page=='troops':
		embed.description=soldiers2(hero)

	elif page=='mats':
		embed.description=mats2(hero)

	elif 'class' in page:
		fields,name=hclass2(hero,int(page[-1]))
		embed.description=fields
		#embed.ConvertFields(fields)
		embed.title=name

	elif page=='skins':
		embeds=[]
		for skin in hero['SkinsID']:
			skin = DIRS2['HeroSkin'][skin]
			embed= Embed(
				title=skin['Name'], #page name
				description=skin['Desc']
			)
			embed.set_thumbnail(url=gitlink(skin['Icon']))
			#image link
			surl = skin['Icon'].split('/')
			surl[-1]='Skin%s'%surl[-1].replace('Skin','').replace('Icon','')
			print('/'.join(surl))
			embed.set_image(url=gitlink('/'.join(surl)))
			embeds.append(embed)
		return embeds
		
	return embed

def	main2(hero):
	fields=[
		#{'name':'Type'   ,'value':hero['EquipmentType'][14:] ,'inline':True},
		{'name':'Rarity'		,'value':   DIRS2['Rank'][hero['Rank']]['Desc'] ,'inline':True},
		#{'name':'Description'   ,'value':   hero['Desc'] ,'inline':True},
		{'name':'Allegiance'  ,'value':   '\n'.join(tag['Name'] for id,tag in DIRS2['HeroTag'].items() if hero['ID'] in tag['RelatedHerosID']), 'inline':True},
	]
	if 'JobConnectionID' in hero:
		talents = DIRS2['JobConnection'][hero['JobConnectionID']]['TalentSkillIDs']
		fields.append({'name':'Talent - %s'%DIRS2['Skill'][talents[-1]]['Name'], 'value': SkillDescriptionAIO(talents[hero["Star"]-1:]), 'inline':False})

	if "UseableJobConnectionsID" in hero:
		jobs={i:'' for i in range(0,7)}
		jobs[0]='%s (%s)'%(DIRS2['Job'][DIRS2['JobConnection'][hero['JobConnectionID']]['JobID']]['Name'],0)
		for i,jid in enumerate(hero['UseableJobConnectionsID']):
			if i<len(hero['UseableJobConnectionsID']):
				if i<3: #T2 job
					jobs[i+1]='%s (%s)'%(DIRS2['Job'][DIRS2['JobConnection'][jid]['JobID']]['Name'],i+1)
				else:
					try:
						pre_index=hero['UseableJobConnectionsID'].index(DIRS2['JobConnection'][jid]['PreJobConnectionList'][0])
						jobs[4+pre_index]=' -> %s (%s)'%(DIRS2['Job'][DIRS2['JobConnection'][jid]['JobID']]['Name'],i+1)
					except:
						pass
						#jobs[i]=' -> %s (%s)'%(DIRS2['Job'][DIRS2['JobConnection'][jid]['JobID']]['Name'],i+1)

		fields.append({
			'name':	'Classes', 'value':   '''
			__Initial Class__
			{0}
			__First Path__
			{1}{4}
			__Second Path__
			{2}{5}
			__Third Path__
			{3}{6}
			'''.format(
				*[val for key,val in jobs.items()]
			)
		})
	return fields

def mats2(hero):
	jcons=[hero['JobConnectionID'], *hero['UseableJobConnectionsID']]
	text=[]
	for jcon in jcons:
		jcon = DIRS2['JobConnection'][jcon]
		
		#job name
		text.append('**%s**'%DIRS2['Job'][jcon['JobID']]['Name'])

		for i,jlv in enumerate(jcon['JobLevelsID']):
			cjlv = DIRS2['JobLevel'][jlv]
			mats=[]
			if cjlv["Materials"]:
				for mat in cjlv["Materials"]:
					if mat['GoodsType'] == 'GoodsType_JobMaterial' and mat['Count']!=999:
						mats.append('%sx %s'%(mat['Count'],DIRS2['JobMaterial'][mat['Id']]['Name']))
			if mats:
				text.append('```%s```'%'\n'.join(mats))
	
	return '\n'.join(text)

def hclass2(hero,num):
	if num == 0:
		jcon = hero['JobConnectionID']
	else:
		jcon = hero['UseableJobConnectionsID'][num-1]

	jcon = DIRS2['JobConnection'][jcon]
	job = DIRS2['Job'][jcon['JobID']]

	fields=[
		{'name':	'Typ',			'value':	'%s [%s]'%(DIRS2['Army'][job['ArmyID']]['Name'], 'Melee' if job['IsMelee'] else 'Ranged'), 'inline':True},
		{'name':	'Move',			'value':	job['MoveType'][9:],	'inline':	True},
		{'name':	'Description',	'value':	job['Desc'],	'inline':	False}
	]
	####	skills and units
	skills=[]
	soldiers=[]
	for i,jlv in enumerate(jcon['JobLevelsID']):
		cjlv = DIRS2['JobLevel'][jlv]
		if 	cjlv["GotSkillID"]:
			skills.append('__%s. %s__\n```%s```'%(i,DIRS2['Skill'][cjlv["GotSkillID"]]['Name'],SkillDescriptionAIO([cjlv["GotSkillID"]]).replace('**','')))
		if cjlv["GotSoldierID"]:
			soldier = DIRS2['Soldier'][cjlv["GotSoldierID"]]
			soldiers.append('__%s. %s [%s]__\n```%s```'%(i,soldier['Name'],DIRS2['Army'][soldier['ArmyID']]['Name'],SkillDescriptionAIO(soldier['SkillsID']).replace('**','') if 'SkillsID' in soldier else '-/-' ))
	if skills:
		fields.append({'name':	'Skills',	'value':	'\n\n'.join(skills),	'inline':	False})
	if skills:
		fields.append({'name':	'Troops',	'value':	'\n\n'.join(soldiers),	'inline':	False})

	return '\n\n'.join(['__**%s**__\n%s'%(item['name'],item['value']) for item in fields]), job['Name']

def skills2(hero):
	jcons=[hero['JobConnectionID'], *hero['UseableJobConnectionsID']]
	skills=[]
	for jcon in jcons:
		jcon = DIRS2['JobConnection'][jcon]
		for jlv in jcon['JobLevelsID']:
			cjlv = DIRS2['JobLevel'][jlv]
			if 	cjlv["GotSkillID"]:
				skills.append((cjlv["GotSkillID"],jcon['JobID']))

	return '\n'.join(
		[
		'__%s [*initial*]__\n```%s```'%(
				DIRS2['Skill'][sid]['Name'],
				SkillDescriptionAIO([sid]).replace('**','')
		)
		for sid in hero['SkillsID']
		]
		+
		[
		'__%s [%s]__\n```%s```'%(
			DIRS2['Skill'][sid]['Name'],
			DIRS2['Job'][jid]['Name'],
			SkillDescriptionAIO([sid]).replace('**','')
		)
		for (sid,jid) in skills
		]
		)

def soldiers2(hero):
	jcons=[hero['JobConnectionID'], *hero['UseableJobConnectionsID']]
	skills=[]
	for jcon in jcons:
		jcon = DIRS2['JobConnection'][jcon]
		for jlv in jcon['JobLevelsID']:
			cjlv = DIRS2['JobLevel'][jlv]
			if 	cjlv["GotSoldierID"]:
				skills.append((cjlv["GotSoldierID"],jcon['JobID']))

	return '\n'.join(
		[
		'__%s [%s]__\n```%s```'%(
			DIRS2['Soldier'][sid]['Name'],
			DIRS2['Job'][jid]['Name'],
			SkillDescriptionAIO(DIRS2['Soldier'][sid]['SkillsID']).replace('**','') if 'SkillsID' in DIRS2['Soldier'][sid] else '-/-'
		)
		for (sid,jid) in skills
		]
		+
		[
		'__%s [*tech*]__\n```%s```'%(
			DIRS2['Soldier'][sid]['Name'],
			SkillDescriptionAIO(DIRS2['Soldier'][sid]['SkillsID']).replace('**','')
		)
		for sid in hero["TechCanLearnSoldiersID"]
		]
		)



def lore2(hero):
	return [
		{'name':	DIRS2["HeroBiography"][bio]["Title"],	'value':	convert_HTML(DIRS2["HeroBiography"][bio]["Desc"]),	'inline':False}
		for bio in DIRS2['HeroInformation'][hero['ID']]["HeroBiographiesID"]
	]




####	old ~ from official wiki database
def Hero(iname, page='main'):
	#get job dir
	hero=DIRS['hero'][iname]

	#create basic embed
	embed= Embed(
		title=page, #page name
		url=hero['url'],
		#color=ELEMENT_COLOR[hero['element']]
	)
	embed.set_author(name=hero['name'], url=embed.url)
	#embed.set_thumbnail(url='http://cdn.alchemistcodedb.com/images/heros/profiles/' + hero["image"] + ".png")
	embed.set_thumbnail(url=hero['img'])

	embed.set_footer(text='hero')

	if page=='main':
		embed.ConvertFields(main(hero))

	elif page=='lore':
		embed.ConvertFields(lore(hero))

	elif page=='image':
		embed.set_image(url=hero['img_full'])

	elif 'class' in page:
		fields,name=hclass(hero,page)
		embed.ConvertFields(fields)
		embed.title=name

	return embed

def main(hero):
	fields=([
		{'name':	'Allegiance','value': hero['details']['Allegiance'],'inline':True},
		{'name':	'Rarity'   ,'value':  hero['rarity'] ,'inline':True},
		{'name':	'Talent - %s'%hero['talent'][0], 'value':	hero['talent'][1],   'inline':True},
		{'name':	'Stats', 'value':	', '.join(['%s %s'%(val,key) for key,val in hero['stats'].items()]),   'inline':True},
		{'name':	'Classes', 'value':   '''
			__Initial Class__
			{0}
			__First Path__
			{1}{4}
			__Second Path__
			{2}{5}
			__Third Path__
			{3}{6}
			'''.format(
				*[
					'%s%s (%s)'%((' -> ' if i>3 else ''),hero['classes'][i]['name'],i+1) if hero['classes'][i]['name'] else ''
					for i in range(0,7)
				]
			)
		}
			
		#{'name':	'', 'value':	hero[''],   'inline':True},
	])
	return fields

def lore(hero):
	order=[
		'Measurements','Height/Weight',
		'Appearance','Allegiance'
	]
	fields=([
		*[
			{'name':key,'value':hero['details'][key],'inline':True}
			for key in order
		],
		{'name':'CV'   ,'value': hero['CV'] ,'inline':True},
		{'name':	'Story', 'value':	hero['story'],   'inline': False},
	])
	return fields

def hclass(hero, page):
	job = hero['classes'][int(page[-1])-1]

	def solderText(soldier):
		text=['**%s**'%soldier['name']]
		if 'expr' in soldier and len(soldier['expr'])>2:
			text.append(soldier['expr'])
		text.append(', '.join(['%s %s'%(val,key) for key,val in soldier['stats'].items()]))
		return '\n'.join(text)
		

	fields=[
		{
			'name': 'Skills',
			'value': '\n\n'.join([
				'**%s**\n%s'%(skl['name'],skl['expr'])
				for skl in job['skills']
			]),
			'inline':False
		},
		{
			'name': 'Soldiers',
			'value': '\n\n'.join([
				solderText(sol)
				for sol in job['soldiers']
			]),
			'inline':False
		}
	]

	return fields,job['name']
