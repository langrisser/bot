from ._main import DIRS2, Embed
from .MapImage import MapImage
from PIL import Image, ImageFont, ImageDraw
import io
from datetime import datetime, timedelta

def JointBattle():
	weekday = (datetime.utcnow() - timedelta(hours=6)).weekday()+1
	for i,jbattle in DIRS2['CooperateBattle'].items():
		if weekday in jbattle["OpenWeekDays"]:
			break
	lvbattle=DIRS2['CooperateBattleLevel'][jbattle['LevelList'][-1]]	#.index(target level ~ for latter additions like enemies)
	battle=DIRS2['Battle'][lvbattle["BattleID"]]

	embed=Embed(
		title=jbattle['Name']
	)
	return embed, MapImage(battle)

if __name__ == '__main__':
	JointBattle()